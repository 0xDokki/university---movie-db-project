package diddi.parser;

// access to fields is needed in diddi.db.model.ModelTransform.java#41
// intellij and/or java can't seem to recognise this.
@SuppressWarnings("WeakerAccess")
public class Director extends Person {
    Director(int id, String name) {
        super(id, name);
    }
}
