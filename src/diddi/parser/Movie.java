package diddi.parser;

import java.util.*;

public class Movie {
    public final int id;
    public final String title;
    public final String plot;
    public final Set<String> genre;
    public final String release;

    // public final Set<Integer> actors;
    // public final Set<Integer> directors;

    Movie(int id, String title, String plot, Set<String> genre, String release) {
        this.id = id;
        this.title = title;
        this.plot = plot;
        this.genre = Collections.unmodifiableSet(genre);
        this.release = release;

        // this.actors = new HashSet<>();
        // this.directors = new HashSet<>();
    }

    @Override
    public String toString() {
        return "[#" + id + "|" + title + "]";
    }
}
