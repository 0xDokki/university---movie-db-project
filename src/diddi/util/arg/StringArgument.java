package diddi.util.arg;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringArgument extends Argument<String> {
    private final Pattern pattern;

    public StringArgument(String longName, String shortName) {
        super(longName, shortName);

        if (this.shortName != null)
            pattern = Pattern.compile(String.format("(%s|%s)=(.*?)( |$)", this.shortName, this.longName));
        else
            pattern = Pattern.compile(String.format("(%s)=(.*?)( |$)", this.longName));
    }

    @Override
    public Set<String> Fetch(String argString) {
        var m = pattern.matcher(argString);

        return m.results()
                .map(r -> r.group(2))
                .collect(Collectors.toSet());
    }
}
