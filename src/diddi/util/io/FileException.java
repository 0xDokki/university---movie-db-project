package diddi.util.io;

import java.io.File;

public class FileException extends Exception {
    private FileException(String s) {
        super(s);
    }

    public static FileException errorOnWrite(File f) {
        return new FileException("Error while writing file '" + f.getAbsolutePath() + "'.");
    }

    public static FileException errorOnRead(File f) {
        return new FileException("Error while reading file '" + f.getAbsolutePath() + "'.");
    }

    public static FileException badContent(File f, String description) {
        return new FileException("Error while reading file '" + f.getAbsolutePath() + "'\n" + description);
    }
}
