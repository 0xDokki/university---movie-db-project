package diddi.db.model;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class DbMovie implements ITriggerableImmutability {
    private final int id;
    private final String title;
    private final String plot;
    private final String release;

    // this isn't final, as it is supposed to change during runtime.
    // NaN specifically denotes 'no rating', but everything <0 and >1 is ignored as well
    private float userRating;

    // these can't be final, as we need to already have the movies to construct
    // all related entities.
    private Set<DbGenre> genres;
    private Set<DbActor> actors;
    private Set<DbDirector> directors;
    private Set<DbRating> ratings;

    public DbMovie(int id, String title, String plot, String release) {
        this.id = id;
        this.title = title;
        this.plot = plot;
        this.release = release;
        userRating = Float.NaN;
    }

    public void fillRelations(Set<DbActor> actors, Set<DbDirector> directors,
                              Set<DbGenre> genres, Set<DbRating> ratings) {
        this.actors = actors;
        this.directors = directors;
        this.genres = genres;
        this.ratings = ratings;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPlot() {
        return plot;
    }

    public String getRelease() {
        return release;
    }

    public float getUserRating() {
        return userRating;
    }

    public void setUserRating(float userRating) {
        this.userRating = userRating;
    }

    public Set<DbGenre> getGenres() {
        return genres;
    }

    public Set<DbActor> getActors() {
        return actors;
    }

    public Set<DbDirector> getDirectors() {
        return directors;
    }

    public Set<DbRating> getRatings() {
        return ratings;
    }

    @Override
    public void makeCollectionFieldsImmutable() {
        genres = Collections.unmodifiableSet(genres);
        actors = Collections.unmodifiableSet(actors);
        directors = Collections.unmodifiableSet(directors);
        ratings = Collections.unmodifiableSet(ratings);
    }

    @Override
    public String toString() {
        return "'" + title + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var dbMovie = (DbMovie) o;
        return id == dbMovie.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
