package diddi.test;

import diddi.util.io.Serialize;
import diddi.util.io.Serializer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class SerializerTest {

    @Test
    @DisplayName("Serializer can serialize and deserialize without throwing an exception.")
    void testDeSerializer() {
        var s = new HashSet<String>();
        s.add("hello");
        s.add("world");

        var m = new HashMap<TestClass, HashSet<String>>();
        m.put(new TestClass(42.1337f, true), s);

        var str = Serializer.serialize(m);
        var br = new BufferedReader(new StringReader(str));

        try {
            br.mark(str.length() + 1);
        } catch (IOException ignored) {
            // fuck checked exceptions at this point
        }

        assertAll(
                () -> assertDoesNotThrow(() -> {
                    Serializer.deserialize(br, m.getClass());
                    br.reset();
                }),
                () -> assertEquals(m, Serializer.deserialize(br, m.getClass()))
        );
    }

    static class TestClass {
        @Serialize("test")
        float test;

        @Serialize("aa")
        boolean aaaa;

        // default constructor needed for deserialization
        public TestClass() {
        }

        TestClass(float test, boolean aaaa) {
            this.test = test;
            this.aaaa = aaaa;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestClass testClass = (TestClass) o;
            return Float.compare(testClass.test, test) == 0 &&
                    aaaa == testClass.aaaa;
        }

        @Override
        public int hashCode() {
            return Objects.hash(test, aaaa);
        }
    }
}
