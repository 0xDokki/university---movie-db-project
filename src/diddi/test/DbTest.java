package diddi.test;

import diddi.db.DB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DbTest {

    @Test
    void test() {
        var model = TestMode.createTestDb();

        var db = new DB(model);

        db.setRatingFile("unit_test.userratings.file");

        assertDoesNotThrow(db::trySaveRatings);
        assertDoesNotThrow(db::tryReadRatings);

        assertEquals(1, db.getRatedMovies().size());

        // welcome to super simple land
        // to the bottom you see the most useless assertion in the history of assertions
        assertEquals(3, db.query().showRecommendations().size());

        // db.find* methods are essentially getters so they're untested
        // query.prefer* and others are essentially setters so they're untested
    }

}
