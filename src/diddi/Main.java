package diddi;

import diddi.db.DB;
import diddi.db.filter.ShitInShitOutFilter;
import diddi.db.model.ModelTransform;
import diddi.interactive_mode.InteractiveMode;
import diddi.parser.Parser;
import diddi.static_mode.StaticMode;
import diddi.test.TestMode;
import diddi.util.arg.StringArgument;

import java.io.File;

class Main {
    public static void main(String[] args) {
        var argString = String.join(" ", args);
        if (argString.contains("help") || argString.contains("-h")) {
            System.out.println(helpText);
            System.exit(0);
        }

        var dbArg = new StringArgument("database", "db").Fetch(argString);
        if (dbArg.size() != 1) {
            System.out.println("Only one database file may be specified");
            System.exit(1);
        }
        var dbFile = dbArg.iterator().next();

        var mergeArg = new StringArgument("merge-genres-on-duplicate-movie-id", null).Fetch(argString);
        var ratingFile = new StringArgument("rating-file", "rf").Fetch(argString);
        var blackListFile = new StringArgument("blacklist-file", "bl").Fetch(argString);

        try {
            var opts = new Parser.ParserOpts();
            opts.mergeGenresOnDuplicateMovieId = mergeArg.size() == 1 && mergeArg.iterator().next().equals("true");
            var res = Parser.parse(new File(dbFile), opts);

            var model = ModelTransform.transformModel(res);

            var shitFilter3000 = new ShitInShitOutFilter();
            shitFilter3000.setBlacklistFile(blackListFile.stream().findFirst().orElse(null));
            model = shitFilter3000.filter(model);

            var db = new DB(model);

            db.setRatingFile(ratingFile.stream().findFirst().orElse(null));
            db.tryReadRatings();

            //noinspection IfCanBeSwitch
            if (args[0].equals("static"))
                StaticMode.start(argString, db);
            else if (args[0].equals("interactive"))
                InteractiveMode.start(db);
            else if (args[0].equals("test"))
                TestMode.start(db);

            db.trySaveRatings();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static final String helpText =
            "Usage:\n" +
            "  Note: if you want to include spaces in any argument,\n" +
            "        you'll have to rely on your terminal to quote or escape them for you.\n" +
            "\n" +
            "  Global options:\n" +
            "    -db --database=<x>        Uses the file x as database.\n" +
            "    -r --ratings=<x>          Uses the file x as storage for personal ratings\n" +
            "    -rf --rating-file=<x>     Uses x to load (if exists) and save user ratings made in interactive mode\n" +
            "    -bl --blacklist-file=<x>  Uses x as the blacklist for filtering the database before making queries\n" +
            "    --merge-genres-on-duplicate-movie-id=true Instructs the parser to merge genres when encountering duplicate movie ids\n" +
            "\n" +
            "  moviedb test\n" +
            "    Invokes testing mode, in which a number of predefined queries are made,\n" +
            "    and the results are written into result.txt\n" +
            "    Additionally, unit tests are run, but their results are only printed to the terminal.\n" +
            "\n" +
            "    Options: global only\n" +
            "\n" +
            "  moviedb static\n" +
            "    Invokes static mode, where a single query based on the given options is made.\n" +
            "\n" +
            "    Options:\n" +
            "      -l --limit=<n>        Limits the output to the n best elements. If multiple are given, the lowest one is used.\n" +
            "      -a --actor=<x>        Only shows movies where all actors x are involved.\n" +
            "      -d --director=<x>     Only shows movies where all directors x are involved.\n" +
            "      -g --genre=<x>        Only shows movies that are tagged with all genres x.\n" +
            "      -f --film=<x>         Bases the film ratings on people who rated all movies x and their ratings\n" +
            "                            instead of global averages\n" +
            "\n" +
            "  moviedb interactive\n" +
            "    Invokes interactive mode, where the database can be viewed and queries made via a command prompt.\n" +
            "\n" +
            "    Options: global only";
}
