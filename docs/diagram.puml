This diagram is written in plantUML. It exists standalone or as a plugin to virtually all IDEs

@startuml

package parser {
    note as n1
        Variable names in all caps
        denotes that it is final, and/or
        is an immutable collection
        for most of its lifetime.
        It can also mean that a private field
        has a public getter, and no setter.
    end note
    note as n2
        A dotted line denotes notable
        internal use. With an arrow,
        it shows usage as parameter or
        return value of a function.
    end note

    abstract class Person {
        +Person(int, String)
        __ Fields __
        +ID : int
        +NAME : string
        __
    }
    class Actor extends Person {
        +Actor(int, String)
        __
    }
    class Director extends Person {
        +Director(int, String)
        __
    }
    class Movie {
        +Movie(int, String, String, Set<String>, String)
        __ Fields __
        +ID : int
        +TITLE : String
        +PLOT : String
        +GENRE : Set<String>
        +RELEASE : String
        __
    }
    class ParserOut {
        __ Fields __
        +ACTORS : Map<int, Actor>
        +DIRECTORS : Map<int, Director>
        +MOVIES : Map<int, Movie>

        +ACTOR_MOVIE_RELATIONS : Map<int, Set<int>>
        +DIRECTOR_MOVIE_RELATIONS : Map<int, Set<int>>

        +MOVIE_RATING_RELATION : Map<int, Map<String, float>>
        __
    }
    class ParserOpts {
        __ Fields __
        +mergeGenresOnDuplicateMovieId : boolean
        __
    }
    class Parser {
        __ Methods __
        +parse(File, ParserOpts) : ParserOut
        +parse(BufferedReader, ParserOpts) : ParserOut
    }

    ParserOut .. n1
    n2 . Parser

    ParserOut *-- "*" Actor
    ParserOut *-- "*" Director
    ParserOut *-- "*" Movie

    Parser <.. ParserOpts
    Parser +-- ParserOpts
    Parser ..> ParserOut
    Parser +-- ParserOut
}

package db {
    package model {
        interface IMovieRelated {
            __ Methods __
            +getRelatedMovies() : Set<DbMovie>
            +isRelatedToMovie(DbMovie) : boolean
        }
        interface ITriggerableImmutability {
            __ Methods __
            +makeCollectionFieldsImmutable() : void
        }
        abstract class DbPerson implements IMovieRelated, ITriggerableImmutability {
            +DbPerson(int, String, Set<DbMovie>)
            __ Fields __
            +ID : int
            +NAME : string
            __ Methods __
            +getRelatedMovies() : Set<DbMovie>
            +isRelatedToMovie(DbMovie) : boolean
            +makeCollectionFieldsImmutable() : void
        }
        class DbActor extends DbPerson {
            +DbActor(int, String, Set<DbMovie>)
            __
        }
        class DbDirector extends DbPerson {
            +DbDirector(int, String, Set<DbMovie>)
            __
        }
        class DbRating {
            +DbRating(String, float, DbMovie)
            __ Fields __
            +USERNAME : String
            +RATING : float
            +MOVIE : DbMovie
            __
        }
        class DbGenre implements IMovieRelated, ITriggerableImmutability {
            +DbGenre(String, Set<DbMovie>)
            __ Fields __
            +NAME : String
            __ Methods __
            +getRelatedMovies() : Set<DbMovie>
            +isRelatedToMovie(DbMovie) : boolean
            +makeCollectionFieldsImmutable() : void
        }
        class DbMovie implements ITriggerableImmutability {
            +DbMovie(int, String, String, String)
            __ Fields __
            +ID : int
            +TITLE : String
            +PLOT : String
            +RELEASE : String

            +ACTORS : Set<DbActor>
            +DIRECTORS : Set<DbDirector>
            +GENRES : Set<DbGenre>
            +RATINGS : Set<DbRating>
            __ Methods __
            +fillRelations(Set<DbActor>, Set<DbDirector>, Set<DbGenre>, Set<DbRating>) : void
            +makeCollectionFieldsImmutable() : void
        }
        class OoModel {
            -OoModel(ParserOut)
            -OoModel(Set<DbMovie, >Set<DbActor>, Set<DbDirector>, Set<DbGenre>, Set<DbRating>)
            __ Fields __
            +MOVIES : Set<DbMovie>
            +ACTORS : Set<DbActor>
            +DIRECTORS : Set<DbDirector>
            +GENRES : Set<DbGenre>
            +RATINGS : Set<DbRating>
            __
        }
        class ModelTransform {
            __ Methods __
            +transformModel(ParserOut) : OoModel
            +createModel(Set<DbMovie, >Set<DbActor>, Set<DbDirector>, Set<DbGenre>, Set<DbRating>) : OoModel
        }

        DbMovie "*" *--* "*" DbActor
        DbMovie "*" *--* "*" DbDirector
        DbMovie "*" *--* "*" DbGenre
        DbRating "*" *--* "1" DbMovie


        ParserOut ..> ModelTransform
        'with regular lines, the transform is a level lower than the model
        'but with short lines, it goes to the other side
        'this is beyond me
        OoModel <.. ModelTransform
        OoModel +-- ModelTransform

        OoModel *-- "*" DbMovie
        OoModel *-- "*" DbActor
        OoModel *-- "*" DbDirector
        OoModel *-- "*" DbGenre
        OoModel *-- "*" DbRating
    }

    package filter {
        class ShitInShitOutFilter {
            __ Methods __
            +setBlacklistFile(String) : void
            +filter(OoModel) : OoModel

            -stage1(BlackList)
            -stage2()
            -stage3(BlackList)
            -stage4()
        }

        class BlackList {
            __ Fields __
            ~deleteMoviesWithoutElements : boolean
            ~movieNameBlacklist : Set<String>
            ~moviePlotBlacklist : Set<String>
            ~movieReleaseBlacklist : Set<String>
            ~actorBlacklist : Set<String>
            ~directorBlacklist : Set<String>
            ~genreBlacklist : Set<String>
            __
        }

        BlackList .. ShitInShitOutFilter
        BlackList --+ ShitInShitOutFilter
        OoModel <..> ShitInShitOutFilter
    }

    class DB {
        +DB(OoModel)
        __ Fields __
        -MODEL : OoModel
        -ratingFile : File
        __ Methods __
        +setRatingFile(String) : void
        +trySaveRatings() : void
        +tryReadRatings() : void
        +getRatedMovies() : Set<DbMovie>

        +findMovies(Collection<String>) : Set<DbMovie>
        +findActors(Collection<String>) : Set<DbActor>
        +findDirectors(Collection<String>) : Set<DbDirector>
        +findGenres(Collection<String>) : Set<DbGenres>
        -containsLike(Collection<String>, String) : boolean

        +query() : Query
    }

    class Query {
        __ Fields __
        -limitActors : Set<DbActor>
        -limitDirectors : Set<DbDirector>
        -limitGenres : Set<DbGenre>
        -ratedLike : Set<DbMovie>
        -maxNum : int
        __ Methods __
        +preferActor(Collection<DbActor>) : Query
        +preferDirector(Collection<DbDirector>) : Query
        +preferGenre(Collection<DbGenre>) : Query
        +ratedLike(Collection<DbMovie>) : Query
        +limit(int) : Query

        +showRecommendations() : LinkedHashMap<DbMovie, float>
        -getRemainingMovieIdsAfterLimitingFactors() : Set<DbMovie>
        -otherPeopleWhoLikedXAlsoLiked() : Map<DbMovie, float>
        -otherPeopleGenerallyLike() : Map<DbMovie, float>
    }

    note bottom: if I add all relations here,\n everything will just explode

    Query --+ DB
    DB ..> Query

    OoModel ..> DB
}

package util {
    package arg {
        abstract class Argument <Type T> {
            ~Argument(String, String)
            __ Fields __
            ~LONGNAME : String
            ~SHORTNAME : String
            __ Methods __
            {abstract}+Fetch(String) : Set<T>
        }

        class StringArgument <String> extends Argument {
            +StringArgument(String, String)
            __ Fields __
            -PATTERN : Pattern
            __ Methods __
            +Fetch(String) : Set<String>
        }

        class IntArgument <Integer> extends Argument {
            +IntArgument(String, String)
            __ Fields __
            -PATTERN : Pattern
            __ Methods __
            +Fetch(String) : Set<Integer>
        }
    }

    package io {
        'Annotations in plantUML can't have a body, so there we go
        annotation Serialize

        class Serializer {
            __ Methods __
            {static}+serialize(Object) : String
            {static}-serializeInternal(String, StringBuilder, Object) : void
            {static}+deserialize<T>(BufferedReader, Class<T>) : T
            {static}-deserializeInternal(Map<String, String>) : Object
            {static}-eatOnePrefix(Map<String, String>) : Map<String, String>
        }

        Serializer .. Serialize
        BlackList .. Serialize
        DB .. Serializer
        ShitInShitOutFilter .. Serializer
    }
}

package static_mode {
    class StaticMode {
        __ Methods __
        {static}+start(String, DB) : void
    }

    StaticMode .. StringArgument
    StaticMode .. IntArgument
}

package interactive_mode {
    class InteractiveMode {
        __ Fields __
        {static}-db : DB
        {static}-input : BufferedReader
        __ Methods __
        {static}+start(DB) : void
        {static}-stateMachineStep(StateMachineStepData) : void
        {static}-queryInput(String) : String
    }

    enum State {
        MODE_SELECT
        QUERY_MASTER
        SEARCH_SELECT
        SEARCH_MOVIE
        SEARCH_ACTOR
        SEARCH_DIRECTOR
        SEARCH_GENRE
        MOVIE_RESULT_LIST
        MOVIE_SELECTED
        FINISH
    }

    class StateMachineStepData {
        ~state : State
        ~data : Object
    }

    StateMachineStepData *- "1" State

    InteractiveMode +-- State
    InteractiveMode +-- StateMachineStepData
}

class Main {
    __ Methods __
    {static}+main(String[] args) : void
}

Main .. StringArgument
Main .. StaticMode
Main .. InteractiveMode
Main .. Parser
Main .. OoModel
Main .. DB
Main .. ShitInShitOutFilter
Main . ModelTransform

@enduml
